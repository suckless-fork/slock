/* user and group to drop privileges to */
static const char *user = "nobody";
static const char *group = "nogroup";

static char *colorname[NUMCOLS] = {
	[INIT] = "black", /* after initialization */
	[INPUT] = "#005577", /* during input */
	[FAILED] = "#CC3333", /* wrong password */
};

/* treat a cleared input like a wrong password (color) */
static int failonclear = 1;

/* Xresources */
Resource resources[] = {
	{ "cinit", STRING, &colorname[INIT] },
	{ "cinput", STRING, &colorname[INPUT] },
	{ "cfailed", STRING, &colorname[FAILED] },
	{ "failonclear", INTEGER, &failonclear },
};
